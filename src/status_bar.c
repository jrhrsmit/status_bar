#include <errno.h>
#include <locale.h>
#include <math.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <time.h>
#include <wchar.h>
#include "wcwidth.h"

#define SCROLL_DIR_FORWARD 0
#define SCROLL_DIR_BACKWARD 1
#define BUFSZ 512

void sleep(double seconds) {
    struct timespec ts;
    ts.tv_sec = (time_t)seconds;
    ts.tv_nsec =
        (long int)((seconds - (double)ts.tv_sec) * (double)1000000000L);
    nanosleep(&ts, 0);
}

wchar_t* fgetws(wchar_t* ws, int n, FILE* stream) {
    int cbuf;
    int i;
    for (i = 0; i < n; i++) {
        cbuf = fgetc(stream);
        // it its a normal ascii character
        if ((cbuf & 0x80) == 0) {
            if (cbuf == '\n') {
                ws[i] = '\0';
                return ws;
            }
            ws[i] = cbuf;
        } else if ((cbuf & 0xE0) == 0xC0) {
            // UTF8 2 bytes to unicode 11 bits
            ws[i] = (cbuf & 0x1F) << 6;
            cbuf = fgetc(stream);
            n++;
            ws[i] |= cbuf & 0x3F;
        } else if ((cbuf & 0xF0) == 0xE0) {
            // UTF8 3 bytes to unicode 16 bits
            ws[i] = (cbuf & 0x0F) << 12;
            cbuf = fgetc(stream);
            n++;
            ws[i] |= (cbuf & 0x3F) << 6;
            cbuf = fgetc(stream);
            n++;
            ws[i] |= cbuf & 0x3F;
        } else if ((cbuf & 0xF8) == 0xF0) {
            // UTF8 4 bytes to unicode 21 bits
            ws[i] = (cbuf & 0x0E) << 18;
            cbuf = fgetc(stream);
            n++;
            ws[i] |= (cbuf & 0x3F) << 12;
            cbuf = fgetc(stream);
            n++;
            ws[i] |= (cbuf & 0x3F) << 6;
            cbuf = fgetc(stream);
            n++;
            ws[i] |= cbuf & 0x3F;
        }
    }
    return ws;
}

void jsonFormat(wchar_t* status, int style, int color_hex, int width) {
    static wchar_t status_tmp[BUFSZ - 64] = {0};
    static int len = BUFSZ;
    // TODO: wcsnlen gives back the number of characters, not the size in bytes
    // right?
    if (wcsnlen(status_tmp, BUFSZ - 1) > BUFSZ - 128) {
        swprintf(status, BUFSZ - 1,
                 L"{\"full_text\":\"Error: output exceeds buffer size of %d\"}",
                 BUFSZ - 128);
        return;
    }
    // wcsncpy(status_tmp, status, BUFSZ);
    memcpy(status_tmp, status, BUFSZ - 65);
    switch (style) {
        case 0:
            swprintf(status, len,
                     L"{\"markup\":\"pango\",\"full_text\":\"%ls\",\"align\":"
                     "\"center\",\"color\":\"#%06X\",\"min_width\":%d}",
                     status_tmp, color_hex, width);
            break;
        case 1:
            swprintf(
                status, len,
                L"{\"markup\":\"pango\",\"full_text\":\"<b>%ls</b>\",\"align\":"
                "\"center\",\"color\":\"#%06X\",\"min_width\":%d}",
                status_tmp, color_hex, width);
            break;
        case 2:
            swprintf(
                status, len,
                L"{\"markup\":\"pango\",\"full_text\":\"<i>%ls</i>\",\"align\":"
                "\"center\",\"color\":\"#%06X\",\"min_width\":%d}",
                status_tmp, color_hex, width);
            break;
        case 3:
            swprintf(status, len,
                     L"{\"markup\":\"pango\",\"full_text\":\"<i><b>%ls</b></"
                     "i>\",\"align\":\"center\",\"color\":\"#%06X\",\"min_"
                     "width\":%d}",
                     status_tmp, color_hex, width);
            break;
        default:
            exit(EXIT_FAILURE);
    }
}

int strUtf8ToInt(int* dst, char* src) {
    int i, j, k;
    j = -1;
    k = 0;
    for (i = 0; src[i]; i++) {
        if (src[i] < 0x7F) {
            // if it's just standard 1 byte ascii
            j++;
            dst[j] = src[i];
        } else if ((src[i] & 0xC0) != 0x80) {
            // if it's the start of a UTF8 series
            j++;
            dst[j] = src[i];
            k = 1;
        } else {
            // else it's the continuation of UTF8
            dst[j] |= src[i] << (k * 8);
        }
    }
    j++;
    return j;
}

int is_space(wchar_t wc) {
    if (wc == 0x0020)
        return 1;
    if (wc < 0x0080)
        return 0;
    if (wc == 0x00A0)
        return 1;
    if (wc == 0x1680)
        return 1;
    if (wc == 0x180E)
        return 1;
    if (wc >= 0x2000 && wc <= 0x200B)
        return 1;
    if (wc == 0x202F)
        return 1;
    if (wc == 0x205F)
        return 1;
    if (wc == 0x3000)
        return 1;
    if (wc == 0xFEFF)
        return 1;
    return 0;
}

wchar_t* scroll(wchar_t* string_input, int width) {
    static wchar_t current_str[BUFSZ];
    static int* str_cwidth;
    static int str_width;
    static int dir = 1;
    static int pos = 0;
    static int end_pos;
    static int str_len = 1;
    int w = 0;
    int i;
    // check if the string is different compared to the last call
    if (wcsncmp(current_str, string_input, str_len) != 0) {
        // reset scrolling
        pos = 0;
        dir = 1;
        // copy new string
        wcsncpy(current_str, string_input, BUFSZ);
        // calculate the width of each character
        free(str_cwidth);
        str_cwidth = (int*)calloc(sizeof(int), str_len);
        for (i = 0; i < str_len; i++) {
            str_cwidth[i] = mk_wcwidth(current_str[i]);
            str_width += str_cwidth[i];
        }
        // check how long the string is, deduct trailing whitespace
        str_len = wcsnlen(current_str, BUFSZ);
        for (i = str_len - 1; i >= 0; i--) {
            if (is_space(current_str[i])) {
                str_len--;
            } else {
                break;
            }
        }
        // set the last starting position (end_pos)
        end_pos = str_len;
        for (i = str_len - 1; i >= 0 && w < width; i--) {
            w += str_cwidth[i];
            end_pos--;
        }
    }
    string_input += pos;
    for (i = 0; w < width && i < str_len; i++) {
        w += str_cwidth[i + pos];
    }
    if (i > 0 && i != str_len) {
        string_input[i] = L'\0';
    }
    // set the direction and the position for the next call
    pos += dir;
    if (dir < 0 && pos < 0) {
        dir = 1;
        pos = 0;
    } else if (dir > 0 && pos > end_pos) {
        dir = -1;
        pos = end_pos;
    }
    return string_input;
}

void escape_ampersand(wchar_t* input) {
    int i;
    int pos = 0;
    int len;
    len = wcsnlen(input, BUFSZ);
    for (i = 0; i < len; i++) {
        pos += wcscspn(input + pos, L"&");
        if (pos >= len) {
            return;
        }
        input[pos] = L' ';
        pos++;
    }
}

void escape_slash(wchar_t* input) {
    int i, j;
    int pos = 0;
    int len;
    len = wcsnlen(input, BUFSZ);
    for (i = 0; i < len; i++) {
        pos += wcscspn(input + pos, L"\\\"");
        if (pos >= len) {
            return;
        }
        for (j = len + i; j >= pos; j--) {
            input[j + 1] = input[j];
        }
        input[pos] = L'\\';
        pos += 2;
    }
}

void getStatusBattery(wchar_t* status_battery) {
    FILE* cmd_output;
    static wchar_t acpi_output[50];
    static wchar_t uevent_output[32];
    static wchar_t time_remaining[10];
    static int flashing_red = 1;
    int time_remaining_h = -1;
    int time_remaining_m;
    int time_remaining_s;
    int pos_percent;
    int power_now_raw;
    int charging;
    double power_now;
    // start the acpi ocmmand
    cmd_output = popen("acpi", "r");
    if (cmd_output) {
        fgetws(acpi_output, BUFSZ, cmd_output);
        // close the command file
        pclose(cmd_output);
    } else
        return;
    cmd_output =
        popen("cat /sys/class/power_supply/BAT0/uevent | grep POWER_NOW", "r");
    if (cmd_output) {
        fgetws(uevent_output, BUFSZ, cmd_output);
        uevent_output[wcscspn(uevent_output, L"=")] = ' ';
        swscanf(uevent_output, L"%*s %d", &power_now_raw);
        power_now = (double)power_now_raw / (double)1e6;
        // close the command file
        pclose(cmd_output);
    } else
        return;
    // check if we're charging or discharging
    charging = wcscspn(acpi_output, L"C") < 30;
    // copy the percentage
    pos_percent = wcscspn(acpi_output, L"%");
    wcsncpy(status_battery, (wchar_t*)(acpi_output + pos_percent - 3), 4);
    if (status_battery[0] == ',')
        status_battery[0] = ' ';
    // copy the power
    swprintf((wchar_t*)(status_battery + 4), BUFSZ, L"  %2.1fW  ", power_now);
    // check if the command gives a time remaining until charged or
    // discharged
    if (acpi_output[pos_percent + 1] != '\n') {
        // copy the time until charged/discharged
        wcsncpy(time_remaining, (wchar_t*)(acpi_output + pos_percent + 3), 8);
        swscanf(time_remaining, L"%d:%d:%d", &time_remaining_h,
                &time_remaining_m, &time_remaining_s);
        if (wcscspn(acpi_output, L"U") > 15) {
            if (time_remaining_h) {
                swprintf((wchar_t*)(status_battery + 12), BUFSZ,
                         L"%2d:%02d:%02d", time_remaining_h, time_remaining_m,
                         time_remaining_s);
            } else {
                swprintf((wchar_t*)(status_battery + 12), BUFSZ, L"   %2d:%02d",
                         time_remaining_m, time_remaining_s);
            }
        }
    }
    // if < 30m, flashing red
    if (time_remaining_h == 0 && time_remaining_m < 30 && charging == 0) {
        if (flashing_red) {
            jsonFormat(status_battery, 1, 0xFF0000, 200);
            flashing_red = 0;
        } else {
            jsonFormat(status_battery, 0, 0xFFFFFF, 200);
            flashing_red = 1;
        }
    }
    // if <1h, red
    else if (time_remaining_h == 0 && charging == 0)
        jsonFormat(status_battery, 0, 0xFF2222, 200);
    // if <2h, yellow
    else if (time_remaining_h == 1 && charging == 0)
        jsonFormat(status_battery, 0, 0xFFFF00, 200);
    // if charged, green
    else if (wcscspn(acpi_output, L"U") < 15)
        jsonFormat(status_battery, 0, 0x00FF00, 200);
    // else white
    else
        jsonFormat(status_battery, 0, 0xFFFFFF, 200);
}

void getStatusSpace(wchar_t* status_space) {
    FILE* cmd_output;
    int pos_start;
    int pos_end;
    cmd_output = popen("df -h | grep '/$'", "r");
    if (cmd_output) {
        fgetws(status_space, BUFSZ, cmd_output);
        pos_start = wcscspn(status_space, L"G") + 1;
        pos_start += wcscspn(status_space + pos_start, L"G") + 1;
        pos_start += wcscspn(status_space + pos_start, L"1234567890");
        pos_end = pos_start + wcscspn(status_space + pos_start, L"G") + 1;
        wcsncpy(status_space, status_space + pos_start, pos_end - pos_start);
        status_space[pos_end - pos_start] = L'\0';
        jsonFormat(status_space, 0, 0xFFFFFF, 70);
        pclose(cmd_output);
    }
}

void getStatusTemp(wchar_t* status_temp) {
    FILE* cmd_output;
    cmd_output = popen("sensors | grep Package", "r");
    if (cmd_output) {
        fgetws(status_temp, BUFSZ, cmd_output);
        wcsncpy(status_temp, status_temp + wcscspn(status_temp, L"+") + 1, 6);
        status_temp[6] = L'\0';
        jsonFormat(status_temp, 0, 0xFFFFFF, 70);
        pclose(cmd_output);
    }
}

void getStatusVolume(wchar_t* status_volume) {
    FILE* cmd_output;
    int pos_percent;
    int pos;
    int muted;
    cmd_output = popen("amixer get Master | grep %", "r");
    if (cmd_output) {
        fgetws(status_volume, BUFSZ, cmd_output);
        pclose(cmd_output);
    } else
        return;
    muted = status_volume[wcsnlen(status_volume, BUFSZ - 1) - 3] == 'f';
    pos_percent = wcscspn(status_volume, L"%");
    if (status_volume[pos_percent - 2] == '[') {
        wcsncpy(status_volume, &status_volume[pos_percent - 1], 2);
        pos = 2;
    } else if (status_volume[pos_percent - 3] == '[') {
        wcsncpy(status_volume, &status_volume[pos_percent - 2], 3);
        pos = 3;
    } else {
        wcsncpy(status_volume, &status_volume[pos_percent - 3], 4);
        pos = 4;
    }
    if (muted) {
        swprintf(&status_volume[pos], BUFSZ, L" muted");
        pos += 6;
        status_volume[pos] = '\0';
        jsonFormat(status_volume, 0, 0xFFFF00, 120);
    } else {
        status_volume[pos] = '\0';
        jsonFormat(status_volume, 0, 0xFFFFFF, 70);
    }
}

#ifdef MPD
void getStatusMpc(wchar_t* status_mpc, wchar_t* status_mpc_playing) {
    FILE* cmd_output;
    wchar_t status_mpc_output[BUFSZ - 1] = {0};
    wchar_t* status_mpc_scroll = NULL;
    cmd_output = popen("mpc --format=\"%artist% - %title%\"", "r");
    if (cmd_output) {
        fgetws(status_mpc_output, BUFSZ, cmd_output);
        if (status_mpc_output[0] != '\0') {
            status_mpc_scroll = scroll(status_mpc_output, 40);
            escape_slash(status_mpc_scroll);
            escape_ampersand(status_mpc_scroll);
            wcscpy(status_mpc, status_mpc_scroll);
        } else {
            status_mpc[0] = '-';
            status_mpc[1] = '\0';
        }
        fgetws(status_mpc_playing, BUFSZ, cmd_output);
        if (status_mpc_playing[2] == 'l') {
            status_mpc_playing[0] = '>';
            status_mpc_playing[1] = '\0';
            jsonFormat(status_mpc_playing, 1, 0xFFFFFF, 50);
        } else {
            status_mpc_playing[0] = '|';
            status_mpc_playing[1] = '|';
            status_mpc_playing[2] = '\0';
            jsonFormat(status_mpc_playing, 1, 0xFFFFFF, 50);
        }
        pclose(cmd_output);
    }
    if (status_mpc_scroll != NULL) {
        jsonFormat(status_mpc, 0, 0xFFFFFF, 340);
    }
}
#endif

#ifdef SPOTIFY
void getStatusSpotify(wchar_t* status_playerctl,
                      wchar_t* status_playerctl_playing) {
    FILE* cmd_output_title;
    FILE* cmd_output_artist;
    FILE* cmd_output_playing;
    static wchar_t title[BUFSZ];
    static wchar_t artist[BUFSZ];
    static wchar_t status_playerctl_output[BUFSZ];
    int stat_len;
    wchar_t* status_playerctl_scroll;
    static const wchar_t failed[] = L"No players found";
    cmd_output_title = popen("playerctl metadata title", "r");
    cmd_output_artist = popen("playerctl metadata artist", "r");
    cmd_output_playing = popen("playerctl status", "r");
    if (cmd_output_title && cmd_output_artist) {
        fgetws(artist, BUFSZ, cmd_output_artist);
        fgetws(title, BUFSZ, cmd_output_title);
        // stat_len = wcsnlen(status_playerctl_output, BUFSZ);
        if (wcsstr(artist, failed)) {
            status_playerctl[0] = '-';
            status_playerctl[1] = '\0';
        } else {
            wcscpy(status_playerctl_output, artist);
            stat_len = wcsnlen(status_playerctl_output, BUFSZ);
            status_playerctl_output[stat_len] = ' ';
            status_playerctl_output[stat_len + 1] = '-';
            status_playerctl_output[stat_len + 2] = ' ';
            wcscpy(&status_playerctl_output[stat_len + 3], title);

            // status_playerctl_scroll = scroll(status_playerctl_output, 40);
            status_playerctl_scroll = status_playerctl_output;
            escape_slash(status_playerctl_scroll);
            escape_ampersand(status_playerctl_scroll);
            wcscpy(status_playerctl, status_playerctl_scroll);
        }
    } else {
        wcscpy(status_playerctl, L"Playerctl failure!");
    }
    pclose(cmd_output_artist);
    pclose(cmd_output_title);
    if (cmd_output_playing) {
        fgetws(status_playerctl_playing, BUFSZ, cmd_output_playing);
        if (wcsstr(status_playerctl_playing, L"Playing")) {
            status_playerctl_playing[0] = '>';
            status_playerctl_playing[1] = '\0';
            jsonFormat(status_playerctl_playing, 1, 0xFFFFFF, 50);
        } else {
            status_playerctl_playing[0] = '|';
            status_playerctl_playing[1] = '|';
            status_playerctl_playing[2] = '\0';
            jsonFormat(status_playerctl_playing, 1, 0xFFFFFF, 50);
        }
    } else {
        wcscpy(status_playerctl, L":(");
    }
    pclose(cmd_output_playing);
    jsonFormat(status_playerctl, 0, 0xFFFFFF, 340);
}
#endif

void getStatusTime(wchar_t* status_time) {
    FILE* cmd_output;
    cmd_output = popen("date '+%Y-%m-%d %_H:%M:%S'", "r");
    if (cmd_output) {
        fgetws(status_time, BUFSZ, cmd_output);
        status_time[wcscspn(status_time, L"\n")] = '\0';
        pclose(cmd_output);
    }
    jsonFormat(status_time, 0, 0xFFFFFF, 200);
}

void getStatusWifi(wchar_t* status_wifi) {
    FILE* cmd_output;
    wchar_t iwgetid_output[200];
    wchar_t freq_output[200];
    wchar_t ip_output[200];
    int i, j, k;
    int pos = 0;
    wchar_t wc_quote[] = L"\"";
    // getting the SSID
    cmd_output = popen("iwgetid", "r");
    if (cmd_output) {
        fgetws(iwgetid_output, BUFSZ, cmd_output);
        if (iwgetid_output[0] == '\n' || iwgetid_output[0] == '\0' ||
            (iwgetid_output[0] & 0xFF) > 0x7F) {
            swprintf(status_wifi, BUFSZ, L"Not connected senpai :(");
            jsonFormat(status_wifi, 0, 0xFFFFFF, 600);
            return;
        }
        i = 1 + wcscspn(iwgetid_output, wc_quote);
        j = i + wcscspn((wchar_t*)(iwgetid_output + i), wc_quote);
        swprintf(status_wifi, BUFSZ, L"<i>");
        pos += 3;
        wcsncpy(status_wifi + pos, iwgetid_output + i, j - i);
        pos += j - i;
        swprintf(status_wifi + pos, BUFSZ, L"</i>");
        pos += 4;
        pclose(cmd_output);
    }
    status_wifi[pos] = ' ';
    pos++;
    status_wifi[pos] = ' ';
    pos++;
    status_wifi[pos] = ' ';
    pos++;
    // getting the frequency
    cmd_output = popen("iwlist wlp1s0 frequency | grep Current", "r");
    if (cmd_output) {
        fgetws(freq_output, BUFSZ, cmd_output);
        i = wcscspn(freq_output, L"=:");
        j = i + wcscspn((wchar_t*)(freq_output + i), L" ");
        if (i >= j || i >= (int)wcsnlen(freq_output, 100) ||
            j >= (int)wcsnlen(freq_output, 100)) {
            wcsncpy(status_wifi + pos, L"?", 1);
            pos += 1;
        } else {
            wcsncpy(status_wifi + pos, freq_output + i + 1, j - i - 1);
            pos += j - i - 1;
        }
        pclose(cmd_output);
    }
    status_wifi[pos] = ' ';
    pos++;
    status_wifi[pos] = 'G';
    pos++;
    status_wifi[pos] = 'H';
    pos++;
    status_wifi[pos] = 'z';
    pos++;
    status_wifi[pos] = ' ';
    pos++;
    status_wifi[pos] = ' ';
    pos++;
    // getting the ip address
    cmd_output = popen("ip addr show wlp1s0 | grep inet\\ ", "r");
    if (cmd_output) {
        fgetws(ip_output, BUFSZ, cmd_output);
        i = 5 + wcscspn(ip_output, L"i");
        j = i + wcscspn((wchar_t*)(ip_output + i), L"/");
        wcsncpy(status_wifi + pos, ip_output + i, j - i);
        pos += j - i;
        pclose(cmd_output);
    }
    status_wifi[pos] = ' ';
    pos++;
    status_wifi[pos] = ' ';
    pos++;
    // getting the speed
    cmd_output = popen("iwlist wlp1s0 rate | grep Current", "r");
    if (cmd_output) {
        fgetws(ip_output, BUFSZ, cmd_output);
        i = 1 + wcscspn(ip_output, L"=");
        k = 1 + wcscspn(ip_output, L":");
        if (i > k) {
            i = k;
            j = 2 + wcscspn((wchar_t*)(ip_output + i), L"/");
            wcsncpy(status_wifi + pos, ip_output + i, j);
            pos += j;
        } else if (i < k) {
            j = 2 + wcscspn((wchar_t*)(ip_output + i), L"/");
            wcsncpy(status_wifi + pos, ip_output + i, j);
            pos += j;
        }
        pclose(cmd_output);
    }
    status_wifi[pos] = ' ';
    pos++;
    status_wifi[pos] = ' ';
    pos++;
    // getting the tx power
    cmd_output = popen("iwlist wlp1s0 txpower | grep Current", "r");
    if (cmd_output) {
        fgetws(ip_output, BUFSZ, cmd_output);
        i = 1 + wcscspn(ip_output, L"(");
        j = i + wcscspn((wchar_t*)(ip_output + i), L")");
        wcsncpy(status_wifi + pos, ip_output + i, j - i);
        pos += j - i;
        pclose(cmd_output);
    }
    status_wifi[pos] = '\0';
    jsonFormat(status_wifi, 0, 0xFFFFFF, 600);
}

int main() {
    wchar_t status[7 * BUFSZ] = {0};
    wchar_t status_music[BUFSZ] = {0};
    wchar_t status_music_playing[BUFSZ] = {0};
    wchar_t status_time[BUFSZ] = {0};
    wchar_t status_battery[BUFSZ] = {0};
    wchar_t status_volume[BUFSZ] = {0};
    wchar_t status_wifi[BUFSZ] = {0};
    wchar_t status_temp[BUFSZ] = {0};
    wchar_t status_space[BUFSZ] = {0};
    int i;
    int first_run = 1;
    setlocale(LC_ALL, "");
    wprintf(L"{\"version\":1}\n[\n");
    while (1) {
        getStatusTemp(status_temp);
#ifdef MPD
        getStatusMpc(status_music, status_music_playing);
#endif
#ifdef SPOTIFY
        getStatusSpotify(status_music, status_music_playing);
#endif
        getStatusBattery(status_battery);
        getStatusVolume(status_volume);
        getStatusTime(status_time);
        getStatusWifi(status_wifi);
        getStatusSpace(status_space);
        if (first_run) {
            swprintf(
                status, BUFSZ, L"[%ls, %ls, %ls, %ls, %ls, %ls, %ls, %ls]\n",
                status_music_playing, status_music, status_wifi, status_volume,
                status_temp, status_space, status_battery, status_time);
            first_run = 0;
        } else {
            swprintf(
                status, BUFSZ, L",[%ls, %ls, %ls, %ls, %ls, %ls, %ls, %ls]\n",
                status_music_playing, status_music, status_wifi, status_volume,
                status_temp, status_space, status_battery, status_time);
        }
        for (i = 0; i < 1; i++) {
            wprintf(L"%ls", status);
        }
        fflush(stdout);
        sleep(1);
    }
    return 0;
}

