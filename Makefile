CC = gcc
IDIR = inc
CFLAGS = -I$(IDIR) -I /usr/include
CFLAGS += -Wall -Wextra 
#CFLAGS += -Werror
CFLAGS += -Ofast
CFLAGS += -std=gnu99
CFLAGS += -DSPOTIFY

LDFLAGS = -L..

ODIR=obj
LDIR=lib
SRCDIR=src

LIBS=-lm
CFLAGS += $(LIBS)

_DEPS = 
DEPS = $(patsubst %,$(IDIR)/%,$(_DEPS))

_OBJ = status_bar.o wcwidth.o
OBJ = $(patsubst %,$(ODIR)/%,$(_OBJ))


$(ODIR)/%.o: $(SRCDIR)/%.c $(DEPS)
	mkdir obj -p
	$(CC) -c -o $@ $< $(CFLAGS)

status_bar: $(OBJ)
	gcc -o $@ $^ $(CFLAGS)

.PHONY: clean

clean:
	rm -f $(ODIR)/*.o *~ core $(INCDIR)/*~ status_bar
